
(ns intermatcher.db
  (:require [clojure.java.jdbc :as jdbc]
            [honeysql.core :as sql]
            [honeysql.helpers :as helpers]
            ))


(def db
  {:classname   "org.sqlite.JDBC"
   :subprotocol "sqlite"
   :subname     "database.db"
   })



(defn create-db []
  (jdbc/db-do-commands db 
                  (jdbc/create-table-ddl :users
                                    [[:email :text "unique"]
                                     [:password :text]
                                     ]
                                    )))
(defn drop-tables
  []
  (jdbc/db-do-commands db (jdbc/drop-table-ddl :users))
  )


(def  testusers
  [{:email "testz@z.com" :password "z"}
   {:email "testa@z.com" :password "a"}
   {:email "testb@z.com" :password "b"}
   ])





;; (insert! db :news testdata)
;; (drop-tables)

;; (insert-multi! db :users users)

;; (def output
;;   (query db "select * from users"))

;; (keys (first output))
;; (:body (first output))

;; (query db (honeycore/format {:select [:*] :from [:users] :where [:= :password "b"]}))
;; (query db (honeycore/format {:select [:*] :from [:users]}))

